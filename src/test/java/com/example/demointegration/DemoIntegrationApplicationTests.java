package com.example.demointegration;

import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoIntegrationApplicationTests {

    @Test
    void contextLoads() {
    }

}
