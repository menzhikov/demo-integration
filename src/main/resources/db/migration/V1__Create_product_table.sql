create table PRODUCT
(
    ID     bigint       not null,
    NAME   varchar(100) not null,
    PRICE  double precision,
    RATING integer
);