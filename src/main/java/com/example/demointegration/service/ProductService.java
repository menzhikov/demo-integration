package com.example.demointegration.service;

import com.example.demointegration.entity.Product;
import com.example.demointegration.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository products;

    public Optional<Product> getProductById(Long id) {
        return products.findById(id);
    }

    public List<Product> getAll() {
        return products.findAll();
    }
}
